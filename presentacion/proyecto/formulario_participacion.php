<?php
//Consulta grupos investigacion
$grupo = new grupo();
$grupos = $grupo->consultar();

//Consultar responsables
$docente = new docente();
$docentes = $docente->consultar();

//registro
if (isset($_POST["buscar"])) {
    $proyecto = new proyecto();
    $proyectos = $proyecto->consultar_participacion($_POST["investigador"]);
?>


    <div class="container">
        <div class="row mt-4">
            <div class="col">
                <div class="card">
                    <h5 class="card-header">Consultar Proyectos</h5>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" rowspan="1" class="text-center">#</th>
                                    <th scope="col" colspan="2" class="text-center table-warning">Informacion</th>
                                    <th scope="col" colspan="2" class="text-center table-success">Fechas</th>
                                    <th scope="col" colspan="2" class="text-center table-info">Encargados</th>
                                </tr>
                                <tr>
                                    <th scope="col">codigo</th>
                                    <th scope="col">nombre</th>
                                    <th scope="col">presupuesto</th>
                                    <th scope="col">Fecha inicio</th>
                                    <th scope="col">fecha final</th>
                                    <th scope="col">grupo</th>
                                    <th scope="col">investigador</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($proyectos as $proyectoActual) {
                                    echo "<tr>";
                                    echo "<td>" . $proyectoActual->getCodigo() . "</td>";
                                    echo "<td>" . $proyectoActual->getNombre() . "</td>";
                                    echo "<td>" . $proyectoActual->getPresupuesto() . "</td>";
                                    echo "<td>" . $proyectoActual->getFecha_inicio() . "</td>";
                                    if ($proyectoActual->getFecha_final() == "0000-00-00") {
                                        echo "<td> En progreso </td>";
                                    } else {
                                        echo "<td>" . $proyectoActual->getFecha_final() . "</td>";
                                    }
                                    foreach ($grupos as $grupoActual) {
                                        if ($proyectoActual->getGrupo() == $grupoActual->getCodigo()) {
                                            echo "<td>" . $grupoActual->getNombre() . "</td>";
                                        }
                                    }
                                    foreach ($docentes as $docenteActual) {
                                        if ($proyectoActual->getInvestigador() == $docenteActual->getcedula()) {
                                            echo "<td>" . $docenteActual->getNombre() . "</td>";
                                        }
                                    }
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
}

?>

<div class="container">
    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="card">
                <h5 class="card-header text-center">Consultar participacion</h5>
                <div class="card-body">
                    <?php if (isset($_POST["buscar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Consulta hecha correctamente
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <!-- formulario post registrar docentes -->
                    <form method="POST" action="index.php?pid=<?php echo base64_encode("presentacion/proyecto/formulario_participacion.php") ?>">
                        <!-- investigadores responsables-->
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Investigadores responsables</label>
                            <select class="form-select" name="investigador">
                                <?php
                                foreach ($docentes as $docenteActual) {
                                    echo "<option value='" . $docenteActual->getcedula() . "'>" . $docenteActual->getNombre() . " - " . $docenteActual->getcedula() . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info" name="buscar">buscar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>