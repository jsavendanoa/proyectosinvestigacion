<?php
//Consulta grupos investigacion
$grupo = new grupo();
$grupos = $grupo -> consultar();

//Consultar responsables
$docente = new docente();
$docentes = $docente -> consultar();

//registro
if( isset($_POST["Registrar"]) ){
    $proyecto = new proyecto($_POST["codigo"], $_POST["nombre"], $_POST["presupuesto"], $_POST["fecha_inicio"], $_POST["fecha_fin"], $_POST["investigador"], $_POST["grupo"],);
    $proyecto -> crear();
}
?>

<div class="container">
    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="card">
                <h5 class="card-header text-center">Registrar proyecto</h5>
                <div class="card-body">
                <?php if(isset($_POST["Registrar"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Proyecto registrado correctamente
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>
                    <!-- formulario post registrar docentes -->
                    <form method="POST" action="index.php?pid=<?php echo base64_encode("presentacion/proyecto/registrarproyecto.php") ?>">

                        <!-- Nombre docente -->
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre" aria-describedby="helpname" required="required">
                        </div>

                        <!-- cedula docente -->
                        <div class="mb-3">
                            <label for="cedula" class="form-label">codigo</label>
                            <input type="number" class="form-control" name="codigo" aria-describedby="helpdecano" required="required">
                            <div id="helpdecano" class="form-text">sin comas ni puntos</div>
                        </div>

                        <!-- experiencia docente -->
                        <div class="mb-3">
                            <label for="presupuesto" class="form-label">Presupuesto</label>
                            <input type="number" class="form-control" name="presupuesto" aria-describedby="helpsede" required="required">
                            <div id="helpsede" class="form-text">experiencia en investigación</div>
                        </div>

                        <!-- fecha de inicio -->
                        <div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Fecha de inicio</label>
							<input type="date" class="form-control" name="fecha_inicio" required="required">
						</div>
                        <!-- fecha de terminacion -->
                        <div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Fecha de terminacion</label>
							<input type="date" class="form-control" name="fecha_fin" ">
						</div>
                        <!-- grupo que lo desarrolla -->
                        <div class="mb-3">	
							<label for="exampleInputEmail1" class="form-label">Grupo que lo desarrolla</label>
    						<select class="form-select" name="grupo">
    							<?php 
    							foreach ($grupos as $grupoActual){
    							    echo "<option value='" . $grupoActual -> getCodigo() . "'>" . $grupoActual -> getNombre() . "</option>";
    							}
    							?>
    						</select>
						</div>
                        <!-- investigadores responsables-->
                        <div class="mb-3">	
							<label for="exampleInputEmail1" class="form-label">Investigadores responsables</label>
    						<select class="form-select" name="investigador">
    							<?php 
    							foreach ($docentes as $docenteActual){
    							    echo "<option value='" . $docenteActual -> getcedula() . "'>" . $docenteActual -> getNombre() . " - " . $docenteActual -> getcedula() . "</option>";
    							}
    							?>
    						</select>
						</div>
                        <button type="submit" class="btn btn-primary" name="Registrar">Registrar</button>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/proyecto/consultarProyecto.php") ?>"> <button type="button" class="btn btn-primary">Consultar</button> </a>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/proyecto/formulario_participacion.php") ?>"> <button type="button" class="btn btn-primary">participacion</button> </a>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/proyecto/proyectos_activos.php") ?>"> <button type="button" class="btn btn-primary mt-2">Activos</button> </a>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/proyecto/proyectos_finalizado.php") ?>"> <button type="button" class="btn btn-primary mt-2">finalizados</button> </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>