<?php
//Consulta grupos investigacion
$grupo = new grupo();
$grupos = $grupo -> consultar();

//Consultar responsables
$docente = new docente();
$docentes = $docente -> consultar();

$proyecto = new proyecto();
$proyectos = $proyecto -> consultar(); 
?>

<div class="container">
	<div class="row mt-4">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Proyectos</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" rowspan="1" class="text-center">#</th>
								<th scope="col" colspan="2" class="text-center table-warning">Informacion</th>
								<th scope="col" colspan="2" class="text-center table-success">Fechas</th>
								<th scope="col" colspan="2" class="text-center table-info">Encargados</th>
							</tr>
							<tr>								
								<th scope="col">codigo</th>
								<th scope="col">nombre</th>
								<th scope="col">presupuesto</th>
								<th scope="col">Fecha inicio</th>
								<th scope="col">fecha final</th>
								<th scope="col">grupo</th>
								<th scope="col">investigador</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($proyectos as $proyectoActual){
							    echo "<tr>";
							    echo "<td>" . $proyectoActual -> getCodigo() . "</td>";
							    echo "<td>" . $proyectoActual -> getNombre() . "</td>";
							    echo "<td>" . $proyectoActual -> getPresupuesto() . "</td>";
								echo "<td>" . $proyectoActual -> getFecha_inicio() . "</td>";
								if($proyectoActual -> getFecha_final() == "0000-00-00"){
									echo "<td> En progreso </td>";
								} else{
									echo "<td>" . $proyectoActual -> getFecha_final() . "</td>";
								}
								foreach ($grupos as $grupoActual){
    							    if($proyectoActual -> getGrupo() == $grupoActual->getCodigo()){
										echo "<td>" . $grupoActual -> getNombre() . "</td>";
									}
    							}
								foreach ($docentes as $docenteActual){
    							    if($proyectoActual -> getInvestigador() == $docenteActual->getcedula()){
										echo "<td>" . $docenteActual -> getNombre() . "</td>";
									}
    							}
							    echo "</tr>";							    
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>