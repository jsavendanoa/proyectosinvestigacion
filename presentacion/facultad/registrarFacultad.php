<?php
$docente = new docente();
$docentes = $docente -> consultar();

if( isset($_POST["Registrar"]) ){
    $facultad = new facultad($_POST["nameFacultad"], $_POST["idDecano"], $_POST["sedeFacultad"]);
    $facultad -> crear();

    $tel_facultad = new tel_facultad($_POST["nameFacultad"] ,$_POST["telefono"]);
    $tel_facultad -> crear();
}
?>

<div class="container">
    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="card">
                <h5 class="card-header text-center">Registrar facultad</h5>
                <div class="card-body">
                <?php if(isset($_POST["Registrar"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Facultad registrada correctamente
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>
                    <!-- formulario post registrar facultades -->
                    <form method="POST" action="index.php?pid=<?php echo base64_encode("presentacion/facultad/registrarFacultad.php") ?>">

                        <!-- Nombre facultad -->
                        <div class="mb-3">
                            <label for="nameFacultad" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nameFacultad" aria-describedby="helpname" required="required">
                            <div id="helpname" class="form-text">Nombre unico de la facultad</div>
                        </div>

                        <!-- docente lider -->
                        <div class="mb-3">	
							<label for="exampleInputEmail1" class="form-label">Lider</label>
    						<select class="form-select" name="idDecano">
    							<?php 
    							foreach ($docentes as $docenteActual){
    							    echo "<option value='" . $docenteActual -> getcedula() . "'>" . $docenteActual -> getNombre() . " - " . $docenteActual -> getcedula() . "</option>";
    							}
    							?>
    						</select>
						</div>

                        <!-- sede facultad -->
                        <div class="mb-3">
                            <label for="sedeFacultad" class="form-label">Sede</label>
                            <input type="text" class="form-control" name="sedeFacultad" aria-describedby="helpsede" required="required">
                            <div id="helpsede" class="form-text">Sede de la facultad</div>
                        </div>

                        <!-- Telefonos facultad-->
                        <div class="mb-3">
                            <label for="nameFacultad" class="form-label">Telefonos</label>
                            <input type="number" class="form-control" name="telefono" aria-describedby="helpname" required="required">
                        </div>
                        <button type="submit" class="btn btn-primary" name="Registrar">Registrar</button>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/facultad/consultarFacultad.php") ?>"> <button type="button" class="btn btn-primary">Consultar</button> </a>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/facultad/presupuesto.php") ?>"> <button type="button" class="btn btn-primary">presupuesto</button> </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>