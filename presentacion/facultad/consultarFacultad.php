<?php
$facultad = new facultad();
$facultades = $facultad -> consultar(); 

$tel_facultad = new tel_facultad();
$tel_facultades = $tel_facultad -> consultar(); 
?>

<div class="container">
	<div class="row mt-4">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Facultades</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" rowspan="1" class="text-center">#</th>
								<th scope="col" colspan="1" class="text-center table-warning">Nombre</th>
								<th scope="col" colspan="1" class="text-center table-success">Cedula decano</th>
								<th scope="col" colspan="1" class="text-center table-info">Sede</th>
								<th scope="col" colspan="1" class="text-center table-warning">Telefonos</th>
							</tr>
							
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($facultades as $facultadActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $facultadActual -> getNombre() . "</td>";
							    echo "<td>" . $facultadActual -> getIdDecano() . "</td>";
							    echo "<td>" . $facultadActual -> getSede() . "</td>";
								foreach($tel_facultades as $tel_facultad){
									echo"<td>" . $tel_facultad -> gettelefono() . "</td>";
								}
							    echo "</tr>";							    
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>