    <div class="container">
        <div class="row mt-3">
            <div class="col-2"></div>
            <div class="col-2 text-center">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/facultad/registrarFacultad.php") ?>"> <button type="button" class="btn btn-primary">Facultades</button> </a>
            </div>

            <div class="col-2 text-center">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/docente/registrarDocente.php") ?>"> <button type="button" class="btn btn-primary">Docentes</button> </a>
            </div>

            <div class="col-2 text-center">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/proyecto/registrarProyecto.php") ?>"> <button type="button" class="btn btn-primary">Proyectos</button> </a>
            </div>

            <div class="col-2 text-center">
                <a href="index.php?pid=<?php echo base64_encode("presentacion/grupo/registrarGrupo.php") ?>"> <button type="button" class="btn btn-primary">Grupos</button> </a>
            </div>

        </div>
    </div>