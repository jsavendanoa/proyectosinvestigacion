<?php
if( isset($_POST["Registrar"]) ){
    $docente = new docente($_POST["nombre"], $_POST["cedula"], $_POST["experiencia"]);
    $docente -> crear();
}
?>

<div class="container">
    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="card">
                <h5 class="card-header text-center">Registrar docente</h5>
                <div class="card-body">
                <?php if(isset($_POST["Registrar"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Docente registrado correctamente :)
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>
                    <!-- formulario post registrar docentes -->
                    <form method="POST" action="index.php?pid=<?php echo base64_encode("presentacion/docente/registrarDocente.php") ?>">

                        <!-- Nombre docente -->
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre" aria-describedby="helpname" required="required">
                        </div>

                        <!-- cedula docente -->
                        <div class="mb-3">
                            <label for="cedula" class="form-label">Cedula</label>
                            <input type="number" class="form-control" name="cedula" aria-describedby="helpdecano" required="required">
                            <div id="helpdecano" class="form-text">sin comas ni puntos</div>
                        </div>

                        <!-- experiencia docente -->
                        <div class="mb-3">
                            <label for="experiencia" class="form-label">Años de experiencia</label>
                            <input type="number" class="form-control" name="experiencia" aria-describedby="helpsede" required="required">
                            <div id="helpsede" class="form-text">experiencia en investigación</div>
                        </div>
                        <button type="submit" class="btn btn-primary" name="Registrar">Registrar</button>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/docente/consultarDocente.php") ?>"> <button type="button" class="btn btn-primary">Consultar</button> </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>