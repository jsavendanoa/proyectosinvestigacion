<?php
$docente = new docente();
$docentes = $docente -> consultar(); 
?>

<div class="container">
	<div class="row mt-4">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Docentes</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" rowspan="1" class="text-center">#</th>
								<th scope="col" colspan="1" class="text-center table-warning">Nombre</th>
								<th scope="col" colspan="1" class="text-center table-success">Cedula</th>
								<th scope="col" colspan="1" class="text-center table-info">Años de experiencia</th>
							</tr>
							
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($docentes as $docenteActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $docenteActual -> getNombre() . "</td>";
							    echo "<td>" . $docenteActual -> getcedula() . "</td>";
							    echo "<td>" . $docenteActual -> getExperiencia() . "</td>";
							    echo "</tr>";							    
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>