<?php
$grupo = new grupo();
$grupos = $grupo -> consultar(); 
?>

<div class="container">
	<div class="row mt-4">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Grupos de investigacion</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" rowspan="1" class="text-center">#</th>
								<th scope="col" colspan="1" class="text-center table-warning">Nombre</th>
								<th scope="col" colspan="1" class="text-center table-success">codigo</th>
								<th scope="col" colspan="1" class="text-center table-info">Lider</th>
								<th scope="col" colspan="1" class="text-center ">Area</th>
								<th scope="col" colspan="1" class="text-center table-success">Factultad</th>
							</tr>
							
						</thead>
						<tbody>
							<?php 
							$i = 1;
							foreach ($grupos as $grupoActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $grupoActual -> getNombre() . "</td>";
								echo "<td>" . $grupoActual -> getCodigo() . "</td>";
								echo "<td>" . $grupoActual -> getLider() . "</td>";
								echo "<td>" . $grupoActual -> getArea() . "</td>";
								echo "<td>" . $grupoActual -> getFacultad() . "</td>"; 
							    echo "</tr>";							    
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>