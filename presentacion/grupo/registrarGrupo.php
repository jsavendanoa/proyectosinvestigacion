<?php
//Carga de docentes
$docente = new docente();
$docentes = $docente -> consultar();

//Carga facultades
$facultad = new facultad();
$facultades = $facultad -> consultar();

if( isset($_POST["Registrar"]) ){
    $grupo = new grupo($_POST["codigo"], $_POST["lider"], $_POST["area"], $_POST["nombre"], $_POST["facultad"]);
    $grupo -> crear();
}
?>

<div class="container">
    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="card">
                <h5 class="card-header text-center">Registrar Grupo</h5>
                <div class="card-body">
                <?php if(isset($_POST["Registrar"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Grupo registrado correctamente
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>
                    <!-- formulario post registrar facultades -->
                    <form method="POST" action="index.php?pid=<?php echo base64_encode("presentacion/grupo/registrarGrupo.php") ?>">

                        <!-- Nombre facultad -->
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre" aria-describedby="helpname" required="required">
                            <div id="helpname" class="form-text">Nombre unico de la facultad</div>
                        </div>

                        <!-- decano facultad -->
                        <div class="mb-3">
                            <label for="codigo" class="form-label">codigo</label>
                            <input type="number" class="form-control" name="codigo" aria-describedby="helpdecano" required="required">
                            <div id="helpdecano" class="form-text">Decano de la facultad</div>
                        </div>

                        <!-- sede facultad -->
                        <div class="mb-3">
                            <label for="area" class="form-label">Area</label>
                            <input type="text" class="form-control" name="area" aria-describedby="helpsede" required="required">
                            <div id="helpsede" class="form-text">Area de interes</div>
                        </div>

                        <!-- docente lider -->
                        <div class="mb-3">	
							<label for="exampleInputEmail1" class="form-label">Lider</label>
    						<select class="form-select" name="lider">
    							<?php 
    							foreach ($docentes as $docenteActual){
    							    echo "<option value='" . $docenteActual -> getcedula() . "'>" . $docenteActual -> getNombre() . " - " . $docenteActual -> getcedula() . "</option>";
    							}
    							?>
    						</select>
						</div>

                        <!-- nombre facultad -->
                        <div class="mb-3">	
							<label for="exampleInputEmail1" class="form-label">Facultad</label>
    						<select class="form-select" name="facultad">
    							<?php 
    							foreach ($facultades as $facultadActual){
    							    echo "<option value='" . $facultadActual -> getNombre() . "'>" . $facultadActual -> getNombre() . "</option>";
    							}
    							?>
    						</select>
						</div>
                        <button type="submit" class="btn btn-primary" name="Registrar">Registrar</button>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/grupo/consultarGrupo.php") ?>"> <button type="button" class="btn btn-primary">Consultar</button> </a>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/grupo/docentesporgrupo.php") ?>"> <button type="button" class="btn btn-primary">docentes</button> </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>