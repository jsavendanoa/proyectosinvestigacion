<?php
//Consulta grupos investigacion
$grupo = new grupo();
$grupos = $grupo->consultar();

//Consultar responsables
$docente = new docente();
$docentes = $docente->consultar();

//registro
if (isset($_POST["buscar"])) {
    $grupo = new grupo();
    $grupos = $grupo->consultarporgrupo();
?>


    <div class="container">
        <div class="row mt-4">
            <div class="col">
                <div class="card">
                    <h5 class="card-header">Consultar Proyectos</h5>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>

                                <tr>
                                    <th scope="col" rowspan="1" class="text-center">#</th>
                                    <th scope="col" colspan="1" class="text-center table-warning">Nombre</th>
                                    <th scope="col" colspan="1" class="text-center table-success">Codigo</th>
                                    <th scope="col" colspan="1" class="text-center table-info">Lider</th>
                                    <th scope="col" colspan="1" class="text-center ">Cedula</th>
                                    <th scope="col" colspan="1" class="text-center table-success">Factultad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($grupos as $grupoActual) {
                                    echo "<tr>";
                                    echo "<td>" . $grupoActual->getNombre() . "</td>";
                                    echo "<td>" . $grupoActual->getCodigo() . "</td>";
                                    echo "<td>" . $grupoActual->getLider() . "</td>";
                                    foreach ($docentes as $docenteActual) {
                                        if($docenteActual -> getcedula() == $grupoActual->getLider()){
                                            echo "<td>" . $docenteActual->getNombre() . "</td>";
                                        }
                                    }
                                    echo "<td>" . $grupoActual->getLider() . "</td>";
                                    echo "<td>" . $grupoActual->getFacultad() . "</td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
}

?>

<div class="container">
    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="card">
                <h5 class="card-header text-center">Consultar participacion</h5>
                <div class="card-body">
                    <?php if (isset($_POST["buscar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Consulta hecha correctamente
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <!-- formulario post registrar docentes -->
                    <form method="POST" action="index.php?pid=<?php echo base64_encode("presentacion/grupo/docentesporgrupo.php") ?>">
                        <!-- investigadores responsables-->
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">grupos</label>
                            <select class="form-select" name="grupo">
                                <?php
                                foreach ($grupos as $grupoActual) {
                                    echo "<option value='" . $grupoActual->getCodigo() . "'>" . $grupoActual->getNombre() . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info" name="buscar">buscar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>