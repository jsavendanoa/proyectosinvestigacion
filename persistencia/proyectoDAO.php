<?php
class proyectoDAO{
    private $nombre;
    private $codigo;
    private $presupuesto;
    private $fecha_inicio;
    private $fecha_final;
    private $grupo;
    private $investigador;

    public function __construct($nombre="", $codigo="", $presupuesto="", $fecha_inicio="", $fecha_final="", $investigador="", $grupo="")
    {
        $this -> nombre = $nombre;
        $this -> codigo = $codigo;
        $this -> presupuesto = $presupuesto;
        $this -> fecha_inicio = $fecha_inicio;
        $this -> fecha_final = $fecha_final;
        $this -> grupo = $grupo;
        $this -> investigador = $investigador;
    }

    public function crear(){
        return "insert into proyecto(codigo, nombre, presupuesto, fecha_inicio, fecha_fin, id_investigador, id_grupo)
            values('". $this -> codigo ."', '". $this -> nombre ."', '". $this -> presupuesto ."' , '". $this -> fecha_inicio ."' , '". $this -> fecha_final ."' , '". $this -> investigador ."' , '". $this -> grupo ."')";
    }

    public function consultar(){
        return "select codigo, nombre, presupuesto, fecha_inicio, fecha_fin, id_investigador, id_grupo
                from proyecto";
    }

    public function consultar_participacion($cedula){
        return "select codigo, nombre, presupuesto, fecha_inicio, fecha_fin, id_investigador, id_grupo
                from proyecto
                where id_investigador = '". $cedula ."' ";
    }

    public function consultar_activos($facultad){
        return "select codigo, nombre, presupuesto, fecha_inicio, fecha_fin, id_investigador, id_grupo
                from proyecto
                where fecha_fin = '0000-00-00' AND id_grupo IN (select id_grupo
                                    from grupo_investigacion
                                    where nombre_facultad = '" . $facultad . "')";
    }

    public function consultar_finalizado($facultad){
        return "select codigo, nombre, presupuesto, fecha_inicio, fecha_fin, id_investigador, id_grupo
                from proyecto
                where fecha_fin != '0000-00-00' AND id_grupo IN (select id_grupo
                                    from grupo_investigacion
                                    where nombre_facultad = '" . $facultad . "')";
    }

    public function presupuesto($facultad, $fecha_inicio, $fecha_fin){
        return "select codigo, nombre, presupuesto, fecha_inicio, fecha_fin, id_investigador, id_grupo
                from proyecto
                where fecha_inicio >= '". $fecha_inicio ."' AND fecha_fin <= '". $fecha_fin ."' AND (select id_grupo
                                                                          from grupo_investigacion
                                                                          where nombre_facultad = '". $facultad ."')";
    }
}
?>