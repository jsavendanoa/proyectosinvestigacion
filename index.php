<?php
session_start();
require_once 'logica/facultad.php';
require_once 'logica/docente.php';
require_once 'logica/proyecto.php';
require_once 'logica/grupo.php';
require_once 'logica/tel_facultad.php';

include 'presentacion/encabezado.php';

$pid = "";
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}
$paginasSinSesion = array(
    "presentacion/facultad/registrarFacultad.php",
    //"presentacion/facultad/consultarFacultad.php",
    "presentacion/inicio.php",
    "presentacion/autenticar.php",
    "presentacion/registrar.php",
    "presentacion/Tiendas.php",

)
?>

<html>

<head>
    <!-- Link Bootstrap CSS para estilos -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<?php
if ($pid != "") {
    //if (in_array($pid, $paginasSinSesion)) {
        include 'presentacion/buscador.php';
        include $pid;
    //} else {
    //}
} else {
    include 'presentacion/buscador.php';
?>
    <div class="container">
        <div class="row mt-3">
            <div class="col-lg-2 text-center"> </div>

            <div class="col-lg-8 text-center">
                <!-- Carrusel img -->

                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/5.jpg" class="d-block w-100" alt="tecno">
                        </div>
                        <div class="carousel-item">
                            <img src="img/4.jpg" class="d-block w-100" alt="40">
                        </div>
                        <div class="carousel-item">
                            <img src="img/2.jpg" class="d-block w-100" alt="fac">
                        </div>
                    </div>
                    <button class="carousel-control-prev" role="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" role="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>

            <div class="col-lg-2 text-center"> </div>
        </div>
    <?php
}
    ?>

    <body>





    </body>

</html>