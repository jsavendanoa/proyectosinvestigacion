<?php   
require_once 'persistencia/Conexion.php';
require_once 'persistencia/docenteDAO.php';

class docente{
    private $nombre;
    private $cedula;
    private $experiencia;
    private $conexion;
    private $docenteDAO;

    public function getNombre(){
        return $this->nombre;
    }

    public function getCedula(){
        return $this->cedula;
    }

    public function getExperiencia(){
        return $this->experiencia;
    }

    public function __construct($nombre="", $cedula="", $experiencia="") {
        $this -> nombre = $nombre;
        $this -> cedula = $cedula;
        $this -> experiencia = $experiencia;
        $this -> conexion = new Conexion();
        $this -> docenteDAO = new docenteDAO($this -> nombre, $this -> cedula, $this -> experiencia);
    }

    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> docenteDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> docenteDAO -> consultar());
        $docentes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $docente = new docente($registro[0], $registro[1], $registro[2]);
            array_push($docentes, $docente);
        }
        $this -> conexion -> cerrar();
        return  $docentes;
    }

}
