<?php   
require_once 'persistencia/Conexion.php';
require_once 'persistencia/grupoDAO.php';

class grupo{
    private $nombre;
    private $codigo;
    private $area;
    private $lider;
    private $facultad;
    private $conexion;
    private $grupoDAO;

    public function getNombre(){
        return $this->nombre;
    }

    public function getCodigo(){
        return $this->codigo;
    }

    public function getArea(){
        return $this->area;
    }

    public function getLider(){
        return $this->lider;
    }

    public function getFacultad(){
        return $this -> facultad;
    }

    public function __construct( $codigo="", $lider="", $area="", $nombre="", $facultad="") {
        $this -> nombre = $nombre;
        $this -> codigo = $codigo;
        $this -> area = $area;
        $this -> lider = $lider;
        $this -> facultad = $facultad;
        $this -> conexion = new Conexion();
        $this -> grupoDAO = new grupoDAO($this -> codigo, $this-> lider, $this -> area, $this -> nombre, $this -> facultad);
    }

    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> grupoDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> grupoDAO -> consultar());
        $grupos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $grupo = new grupo($registro[0], $registro[1], $registro[2], $registro[3], $registro[4]);
            array_push($grupos, $grupo);
        }
        $this -> conexion -> cerrar();
        return  $grupos;
    }

    public function consultarporgrupo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> grupoDAO -> consultar());
        $grupos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $grupo = new grupo($registro[0], $registro[1], $registro[2], $registro[3], $registro[4]);
            array_push($grupos, $grupo);
        }
        $this -> conexion -> cerrar();
        return  $grupos;
    }
}
?>