<?php   
require_once 'persistencia/Conexion.php';
require_once 'persistencia/aporteDAO.php';

class aporte{
    private $id;
    private $id_docente;
    private $fecha_inicio;
    private $fecha_fin;
    private $dedicacion;
    private $conexion;
    private $aporteDAO;

    public function getid(){
        return $this->id;
    }

    public function getid_docente(){
        return $this->id_docente;
    }

    public function getFecha_inicio(){
        return $this->fecha_inicio;
    }

    public function getfecha_fin(){
        return $this->fecha_fin;
    }

    public function getdedicacion(){
        return $this->dedicacion;
    }

    public function __construct($id="", $id_docente="", $fecha_inicio="",$fecha_fin="",$dedicacion="") {
        $this -> id = $id;
        $this -> id_docente = $id_docente;
        $this -> fecha_inicio = $fecha_inicio;
        $this -> fecha_fin = $fecha_fin;
        $this -> dedicacion = $dedicacion;
        $this -> conexion = new Conexion();
        $this -> aporteDAO = new aporteDAO($this -> id, $this -> id_docente, $this -> fecha_inicio, $this -> fecha_fin, $this -> dedicacion);
    }

    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aporteDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> aporteDAO -> consultar());
        $aportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $aporte = new aporte($registro[0], $registro[1]);
            array_push($aportes, $aporte);
        }
        $this -> conexion -> cerrar();
        return  $aportes;
    }
}
?>