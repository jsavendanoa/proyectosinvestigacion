<?php   
require_once 'persistencia/Conexion.php';
require_once 'persistencia/tel_facultadDAO.php';

class tel_facultad{
    private $nombre;
    private $telefono;
    private $conexion;
    private $tel_facultadDAO;

    public function getNombre(){
        return $this->nombre;
    }

    public function gettelefono(){
        return $this->telefono;
    }

    public function __construct($nombre="", $telefono="") {
        $this -> nombre = $nombre;
        $this -> telefono = $telefono;
        $this -> conexion = new Conexion();
        $this -> tel_facultadDAO = new tel_facultadDAO($this -> nombre, $this -> telefono);
    }

    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tel_facultadDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> tel_facultadDAO -> consultar());
        $tel_facultades = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $tel_facultad = new tel_facultad($registro[0], $registro[1]);
            array_push($tel_facultades, $tel_facultad);
        }
        $this -> conexion -> cerrar();
        return  $tel_facultades;
    }
}
?>