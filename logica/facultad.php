<?php   
require_once 'persistencia/Conexion.php';
require_once 'persistencia/facultadDAO.php';

class facultad{
    private $nombre;
    private $idDecano;
    private $sede;
    private $conexion;
    private $facultadDAO;

    public function getNombre(){
        return $this->nombre;
    }

    public function getIdDecano(){
        return $this->idDecano;
    }

    public function getSede(){
        return $this->sede;
    }

    public function __construct($nombre="", $idDecano="", $sede="") {
        $this -> nombre = $nombre;
        $this -> idDecano = $idDecano;
        $this -> sede = $sede;
        $this -> conexion = new Conexion();
        $this -> facultadDAO = new facultadDAO($this -> nombre, $this -> idDecano, $this -> sede);
    }

    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facultadDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> facultadDAO -> consultar());
        $facultades = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $facultad = new facultad($registro[0], $registro[1], $registro[2]);
            array_push($facultades, $facultad);
        }
        $this -> conexion -> cerrar();
        return  $facultades;
    }

}
?>