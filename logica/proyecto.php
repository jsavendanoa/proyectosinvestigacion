<?php   
require_once 'persistencia/Conexion.php';
require_once 'persistencia/proyectoDAO.php';

class proyecto{
    private $nombre;
    private $codigo;
    private $presupuesto;
    private $fecha_inicio;
    private $fecha_final;
    private $grupo;
    private $investigador;
    private $conexion;
    private $proyectoDAO;

    public function getNombre(){
        return $this->nombre;
    }

    public function getCodigo(){
        return $this->codigo;
    }

    public function getPresupuesto(){
        return $this->presupuesto;
    }

    public function getFecha_inicio(){
        return $this->fecha_inicio;
    }

    public function getFecha_final(){
        return $this->fecha_final;
    }

    public function getGrupo(){
        return $this->grupo;
    }

    public function getInvestigador(){
        return $this->investigador;
    }

    public function __construct($codigo="", $nombre="", $presupuesto="", $fecha_inicio="", $fecha_final="", $investigador="", $grupo="") {
        $this -> nombre = $nombre;
        $this -> codigo = $codigo;
        $this -> presupuesto = $presupuesto;
        $this -> fecha_inicio = $fecha_inicio;
        $this -> fecha_final = $fecha_final;
        $this -> grupo = $grupo;
        $this -> investigador = $investigador;
        $this -> conexion = new Conexion();
        $this -> proyectoDAO = new proyectoDAO($this -> nombre, $this -> codigo, $this -> presupuesto, $this -> fecha_inicio, $this -> fecha_final, $this -> investigador, $this -> grupo);
    }

    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> proyectoDAO -> consultar());
        $proyectos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $proyecto = new proyecto($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($proyectos, $proyecto);
        }
        $this -> conexion -> cerrar();
        return  $proyectos;
    }

    public function consultar_participacion($cedula){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> proyectoDAO -> consultar_participacion($cedula));
        $proyectos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $proyecto = new proyecto($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($proyectos, $proyecto);
        }
        $this -> conexion -> cerrar();
        return  $proyectos;
    }

    public function consultar_activos($facultad){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> proyectoDAO -> consultar_activos($facultad));
        $proyectos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $proyecto = new proyecto($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($proyectos, $proyecto);
        }
        $this -> conexion -> cerrar();
        return  $proyectos;
    }

    public function consultar_finalizado($facultad){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> proyectoDAO -> consultar_finalizado($facultad));
        $proyectos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $proyecto = new proyecto($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($proyectos, $proyecto);
        }
        $this -> conexion -> cerrar();
        return  $proyectos;
    }

    public function presupuesto($facultad, $fecha_inicio, $fecha_fin){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> proyectoDAO -> presupuesto($facultad, $fecha_inicio, $fecha_fin));
        $proyectos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $proyecto = new proyecto($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($proyectos, $proyecto);
        }
        $this -> conexion -> cerrar();
        return  $proyectos;
    }
}
